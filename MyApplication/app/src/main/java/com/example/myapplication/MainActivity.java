package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private EditText r;
    private EditText g;
    private EditText b;
    private View colorView;
    private TextView colorText;
    private Button button;
    private Button butroConsulta;
    private TextView textHora;
    private ProgressBar progress;


    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compositeDisposable = new CompositeDisposable();
        r = findViewById(R.id.r);
        g = findViewById(R.id.g);
        b = findViewById(R.id.b);
        textHora = findViewById(R.id.textHora);
        progress = findViewById(R.id.progress);


        button = findViewById(R.id.btnConverter);
        colorView = findViewById(R.id.color);
        colorText = findViewById(R.id.textColor);
        butroConsulta = findViewById(R.id.butroConsulta);


        button.setOnClickListener(v -> {
            int red = Integer.parseInt(r.getText().toString());
            int green = Integer.parseInt(g.getText().toString());
            int blue = Integer.parseInt(b.getText().toString());
            if (red >= 0 && red <= 255 && green >= 0 && green <= 255 && blue >= 0 && blue <= 255) {
                String colorHex = converterRGBToHexadecimal(red, green, blue);
                colorView.setBackgroundColor(Color.parseColor(colorHex));
                colorText.setText(colorHex);
            } else {
                Toast.makeText(this, "El valor ingresado debe ser de 0 a 255", Toast.LENGTH_LONG).show();
            }
        });

        butroConsulta.setOnClickListener(v->{
                getZonaHoraria();
        });



}

    private String converterRGBToHexadecimal(Integer r, Integer g, Integer b) {
        return String.format("#%02x%02x%02x", r, g, b);
    }

    private void getZonaHoraria(){
        compositeDisposable.add(ApiUtils.Companion.getApiService().getTimeZone()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( timeZone -> {
                    textHora.setText(timeZone.getTimezone());
                        }, throwable -> {
                            Toast.makeText(this, "Ocurrio un error al obtner la zona.Vuelva a intetarlo", Toast.LENGTH_LONG).show();
                    Log.e("Main", throwable.getCause().toString());
                        }

                ));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}