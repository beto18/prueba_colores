package com.example.myapplication

import com.example.myapplication.models.TimeZone
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService{

    @GET("/api/timezone/America/Mexico_City")
    fun getTimeZone(): Single<TimeZone>

}


object RetrofitHelper {

        fun getReftrofit() : Retrofit? {
           return Retrofit.Builder()
                .baseUrl("http://worldtimeapi.org/").
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

}