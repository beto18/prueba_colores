package com.example.myapplication.models

import java.util.*

class TimeZone(
    val datetime: Date,
    val day_of_week: Int,
    val day_of_year: Int,
    val dst: Boolean,
    val dst_from: Date,
    val dst_offset: Int,
    val dst_until: Date,
    val raw_offset: Int,
    val timezone: String,
    val unixtime: Int,
    val utc_datetime: Date,
    val utc_offset: String,
    val week_number: Int
) {
}